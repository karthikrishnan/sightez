import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { environment } from 'src/environments/environment';
import { ProjectsService } from '../../services/projects.service';

@Component({
  selector: 'app-manage-projects',
  templateUrl: './manage-projects.component.html',
  styleUrls: ['./manage-projects.component.css']
})
export class ManageProjectsComponent implements OnInit {
  bbRepos: any = [];
  bbConnStat: boolean;
  bbTooltip: string;
  bbConnString: string;
  reposStat: any = {};
  bbClientId;
  addedBBRepos = [];

  githubRepoList = [];
  gitlabRepoList = [];

  constructor(private projectsService: ProjectsService, private notificationService: NotificationService) { }

  ngOnInit() {
    this.bbClientId = environment.bitbucketConfig.clientId;
    this.getBBConnectionStatus();
    // this.getReposOverview();
    console.log(this.bbClientId);
  }

  connectGithub() {
    window.location.href = 'https://github.com/login/oauth/authorize?client_id=0af3b8a255721f7f2578';
  }

  connectOrDisconnectGitHub() {
    this.notificationService.info({ title: '', message: 'This Feature is coming soon' });
  }

  connectOrDisconnectGitLab() {
    this.notificationService.info({ title: '', message: 'This Feature is coming soon' });
  }

  connectBitbucket() {
    console.log(this.bbClientId);
    window.location.href = 'https://bitbucket.org/site/oauth2/authorize?client_id=885SaJDMW2AyZE8jWH&response_type=code';
    // if (!this.bbConnStat) {
    //   window.location.href = 'https://bitbucket.org/site/oauth2/authorize?client_id=' + this.bbClientId + '&response_type=code';
    // } else {
    //   this.notificationService.success({ title: 'Bitbucket Connection Status', message: 'Already connected with bitbucket.' });
    // }
  }

  addProjects(event, id) {
    if (event.target.checked) {
      this.addedBBRepos.push(id);
    } else {
      if (this.addedBBRepos.indexOf(id) >= 0) {
        this.addedBBRepos.splice(this.addedBBRepos.indexOf(id), 1);
      }
    }
  }

  resyncBitbucketLink() {
    this.projectsService.resyncBitbucketLink().subscribe(res => {
      this.notificationService.success({ title: 'Bitbucket Resync Status', message: res.message });
      this.getBBRepos();
    });
  }

  getBBConnectionStatus() {
    this.projectsService.getBBConnStatus().subscribe(res => {
      if (res.status === 'Y') {
        this.bbConnStat = true;
        this.bbConnString = 'Connected';
        this.bbTooltip = 'Click to Disconnect!';
        this.getBBRepos();
      }

      if (res.status === 'N') {
        this.bbConnStat = false;
        this.bbConnString = 'Connect';
      }

    });
  }

  // get bitbucket repository details
  getBBRepos() {
    this.projectsService.getBBRepos().subscribe(res => {
      this.bbRepos = res;
    });
  }

  // connectBitbucketRepo(id) {
  //   this.dashboardService.connectBitbucketRepo(id).subscribe(res => {
  //     const conStatus$ = this.dashboardService.getRepoConnectionStatus(id).subscribe(resp => {
  //       if (resp.isConnected) {
  //         conStatus$.unsubscribe();
  //         this.getBBRepos();
  //         // this.getReposOverview();
  //       }
  //     });
  //     this.notificationService.success({ title: 'Repository Connection Status', message: res.message });
  //   });
  // }

  saveBBRepos() {
    if (this.addedBBRepos.length > 0) {
      this.addedBBRepos.forEach(element => {
        this.projectsService.connectBitbucketRepo(element).subscribe();
      });
      this.addedBBRepos = [];
      this.notificationService.success({ title: 'Repository Connection Status', message: 'Connecting repository is in progress. It will be updated after fetching all commits.' });
    } else {
      this.notificationService.info({ title: 'Info!', message: 'Please select repositories to save.' });
    }
  }

  enableRefreshBtn() {
    for (let i = 0; i < this.bbRepos.length; i++) {
      if (this.bbRepos[i].connected === true) {
        return true;
      }
    }
    return false;
  }

  refreshBBRepos() {
    for (let i = 0; i < this.bbRepos.length; i++) {
      if (this.bbRepos[i].connected === true) {
        this.syncBitbucketRepo(this.bbRepos[i].id);
      }
    }
  }

  syncBitbucketRepo(repoId) {
    this.projectsService.syncBitbucketRepo(repoId).subscribe(res => {
      this.getBBRepos();
      this.notificationService.success({ title: 'Repository Connection Status', message: res.message });
    });
  }

  // getReposOverview() {
  //   this.dashboardService.getReposOverview().subscribe(res => {
  //     this.reposStat.repoCount = res.repoCount;
  //     this.reposStat.commits = 0;
  //     this.reposStat.linesAdded = 0;
  //     this.reposStat.linesRemoved = 0;
  //     this.reposStat.contributors = res.repoStat.length;

  //     res.repoStat.forEach(element => {
  //       this.reposStat.commits += element.commits;
  //       this.reposStat.linesAdded += element.linesAdded;
  //       this.reposStat.linesRemoved += element.linesRemoved;
  //     });
  //   });
  // }

}
